# FAQ Collapse

Good for creating the FAQ for my community. 🤔

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
